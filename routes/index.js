var express = require('express');
var router = express.Router();
var twitter = require('ntwitter');
var sentiment = require('sentiment');
var stopwords = require('nltk-stopwords');
var english = stopwords.load('english')
const publicIp = require('public-ip');

var ipV4;
publicIp.v4().then(ip => {
    ipV4 = ip;
});

var client = new twitter({
    consumer_key: 'mM2MPq9rjqpw8d2RsvnA8DfIU',
    consumer_secret: 'CFBUoNEU4IvEPghVPa3wztqXGN1hobAexobWaO2nBGxbikJou5',
    access_token_key: '1376889200-huqxbEbSpvqEODHORirld3aYoUs3RRcnwtUwrj1',
    access_token_secret: 'OgEvEu4AJsTog1WKEBjBaqMC2lj5eb6AqWHSqKPQBRkDs'
});

router.get('/', function(req, res) {
    res.render('select', {
        ipV4: ipV4
    });
});

router.get('/home/:ip', function(req, res) {
    res.render('index', {
        keyword: '',
        ip: req.params.ip
    });
});

router.get(['/tweet/:ip/:keyword'], function(req, res) {
    var keyword = req.params.keyword.split(',');
    var tweetText;
    var tweetCounter = 0;
    var sentimentCounter = 0;
    var sentimentScore = 0;
    var totalScore = 0;
    var avgScore = 0;
    var posCounter = 0;
    var neutCounter = 0;
    var negCounter = 0;
    var reaction;
    var tweetSplit = [];
    var tweetStorage = [];

    res.render('index', {
        keyword: keyword,
        ip: req.params.ip
    });


    client.verifyCredentials(function(error, data) {
        if (error) {
            console.log("Error: " + error);
            res.io.emit("tweet", {
                tweet: "<font color='red'>ERROR: " + error.statusCode + "</font>",
                counter: 0,
            });
        }
        var stream = client.stream('statuses/filter', {'track': keyword},
            function(stream) {
                stream.on('data', function(tweet) {
                    tweetText = tweet.text;
                    if (typeof tweet.retweeted_status == 'undefined' &&
                    typeof tweetText != "undefined" &&
                    tweet.lang == 'en') {

                        // Most common words
                        var x = tweetText.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
                        var y = x.replace(/[~`!#@$%^&*(){}\[\];:"'<,.>?\/\\|_+=-]/g, '');
                        var z = y.replace(/([\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2694-\u2697]|\uD83E[\uDD10-\uDD5D])/g, '')
                        tweetSplit = z.toLowerCase().split(' ');
                        for(var i = 0; i < stopwords.remove(tweetSplit, english).length; i++) {
                            var item = stopwords.remove(tweetSplit, english)[i];
                            tweetStorage.push(item);
                        }

                        Array.prototype.sortByFreq = function() {
                            var itm, a = [], L = this.length, o = {};
                            for(var i = 0; i < L; i++) {
                                itm = this[i];
                                if (!itm) continue;
                                if (o[itm] == undefined) o[itm] = 1;
                                else ++o[itm];
                            }
                            for(var p in o) a[a.length] = p;
                            return a.sort(function(a, b) {
                                return o[b] - o[a];
                            });
                        }


                        // Sentiment Analysis
                        sentiment(tweetText, function(err, result) {
                            sentimentScore = result.score;
                            sentimentCounter++;
                            totalScore += sentimentScore;
                            avgScore = (totalScore / sentimentCounter).toFixed(2);

                            if (sentimentScore > 0) posCounter++;
                            else if (sentimentScore == 0) neutCounter++;
                            else if (sentimentScore < 0) negCounter++;

                            if (avgScore > 0.5) reaction =
                                "<font color='green'> happy :) </font>";
                            else if (avgScore < -0.5) reaction =
                                "<font color='red'> upset :( </font>";
                            else reaction =
                                    "<font color='blue'> neutral :p </font>";
                        });

                        tweetCounter++;
                        res.io.emit("tweet", {
                            keyword: keyword,
                            tweet: '<i>' + keyword + '#' + tweetCounter + '</i> - ' + tweetText,
                            counter: tweetCounter,
                            sentimentCounter: sentimentCounter,
                            avgScore: avgScore,
                            posCounter: posCounter,
                            neutCounter: neutCounter,
                            negCounter: negCounter,
                            reaction: reaction,
                            commonWords: tweetStorage.sortByFreq(),
                        });
                    }
                });

                stream.on('error', function(error) {
                    console.log("Error: " + error);
                    res.io.emit("tweet", {
                        keyword: '',
                        tweet: "<font color='red'>ERROR: " + error + "</font>",
                        counter: 0,
                    });
                });
            });
    });
});


module.exports = router;
